import os
import sys
import time
import playsound

class bcolors:
	HEADER = '\033[95m'
	OKBLUE = '\033[94m'
	OKGREEN = '\033[92m'
	WARNING = '\033[93m'
	FAIL = '\033[91m'
	ENDC = '\033[0m'
	BOLD = '\033[1m'
	UNDERLINE = '\033[4m'

print bcolors.HEADER + "Initialized" + bcolors.ENDC

if len(sys.argv) < 2:
	print bcolors.FAIL + "[!] " + "usage: python ping.py [hostname | ip adress]" + bcolors.ENDC
	sys.exit()

response = 1
hostname = sys.argv[1]

while response != 0:
	print bcolors.OKGREEN
	response = os.system("ping -c 1 " + hostname)
	print bcolors.ENDC

	time.sleep(1)

	if response == 0:
		playsound.playsound('./anthem.mp3', True)
		break